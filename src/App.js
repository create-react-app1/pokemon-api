import React, {Component} from 'react';
import PropTypes from 'prop-types'

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { clickButton } from './actions';

// Style
import './App.css';

// Components
import PokemonCardList from './components/pokemon-card-list'

function App() {
  return (
    <div className="App" style={{ paddingTop: '10px' }}>
      <PokemonCardList />
    </div>
  )
}

const mapStateToProps = store => ({
  newValue: store.clickState.newValue
})

const mapDispatchToProps = dispatch => 
  bindActionCreators({
    clickButton
  }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(App)