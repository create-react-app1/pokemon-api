import React from 'react'
import './style.css'

import defaultImage from '../../images/interrogation_signal.png'

function PokemonCard ({pokemon}) {
  return (
    <div className="pokemon-card">
      <img src={pokemon.sprites.front_default || defaultImage}/>
      <div className="pokemon-title">{pokemon.id} - {pokemon.name}</div>
    </div>
  )

}

export default PokemonCard