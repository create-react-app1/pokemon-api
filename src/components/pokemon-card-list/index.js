import React, {useState, useEffect} from 'react'
import './style.css'

import pokemonAPI from '../../services/pokemonsAPI'

import PokemonCard from '../pokemon-card'

import pokemonLogo from '../../images/pokemon_logo.png'
import loadingGIF from '../../images/loading.gif'

function PokemonCardList () {

  const [pokemons, addPokemons] = useState([])
  const [totalCount, setCountTotal] = useState(0)
  const [currentCount, setCurrentCount] = useState(0)
  const [nextUrl, setNextUrl] = useState(null)

  const [loading, setLoading] = useState(true)

  useEffect(() => {
    getPokemons()
  }, [totalCount])

  async function getPokemons() {
    setLoading(true)
    try {
      const response = await pokemonAPI.get('pokemon' + (nextUrl || ''))

      const { count, next, results } = response.data
      setCountTotal(count)
      setCurrentCount(currentCount + results.length)
      next && setNextUrl(next.replace('https://pokeapi.co/api/v2/pokemon', ''))
      
      // CHALLENGE ***
      // Try to use Promisse All Resolved
      let pokemonsToAdd = []
      for(let i = 0; i < results.length; i++) {
        pokemonsToAdd.push(await getPokemonsByName(results[i].name))
      }
      addPokemons(pokemons.concat(pokemonsToAdd))
    }
    catch(e) {
      console.warn(e)
      alert("Problems for get pokemon list...")
    }
    setLoading(false)
  }

  async function getPokemonsByName(name) {
    let pokemon = { name }
    try {
      const response = await pokemonAPI.get(`pokemon/${name}/`)

      const { id, sprites } = response.data
      pokemon = {
        ...pokemon,
        id, 
        sprites
      }      
    }
    catch(e) {
      console.warn("Problems for get informations about ", name, e)
    }
    return pokemon
  }

  return (
    <div className="pokemon-card-list">
      <img className="pokemon-logo" src={pokemonLogo} />
      <label>{currentCount} / {totalCount}</label>
      <div className="pokemon-card-list-container">
        { pokemons.map(p => <PokemonCard key={p.id} pokemon={p}/>) }
      </div>
      <label>{currentCount} / {totalCount}</label>
      { currentCount < totalCount && 
        <button className={loading ? 'disabled' : ''} onClick={getPokemons}>
          {loading ? <img src={loadingGIF}/> : "Next"}
        </button> 
      }
    </div>
  )

}

export default PokemonCardList